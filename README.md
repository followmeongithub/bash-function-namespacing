# BASH Function Namespacing



A simple example of applying namespacing to sourced functions in BASH.
This can be useful if multiple sourced scripts have functions with the
same name or for better readability.

Instead of calling the function `do_something` from `lib1`, you can call
the sourced function with `lib1.do_something`.

**Note:** Due to use of dot `.` notation, this example is **NOT**
POSIX compliant. Your mileage may vary in other shells. Simply updating
the renaming line of the `namespace` function to replace `.` with `_`
_should_ be POSIX compliant.

Example run:

```
user@b07b886eadb5:/# ./main
Namespacing the following functions in lib1: do_something
Namespacing the following functions in lib2: do_nothing do_something
I am lib1 and I'm doing something...

I'm doing something...
Zzzz...

Zzzz...

```
